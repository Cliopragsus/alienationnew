﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "M/Neon2"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Noise("Texture", 2D) = "white" {}
		_Color1("Color",color) = (1,1,1,1)
		_Height("Height of bands", Range(0,0.4)) = 0.1
		_Transp("Transparency", Range(0,1)) = 0.9
		_Transp2("Transparency2", Range(0,1)) = 0.9
		_Speed("Speed",Range(0,0.2)) = 0.03
		_Strength("Strengtg",Range(1,100)) = 30
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			half3 Overlay(half3 a, half3 b)
			{
				return (a < 0.5) ? (2 * a*b) : (1 - 2 * (1 - a)*(1 - b));
			};

			sampler2D _MainTex;
			sampler2D _Noise;
			float4 _MainTex_ST;
			half4 _Color1;
			half _Height;
			half _Transp;
			half _Transp2;
			half _Speed;
			half _Strength;

			v2f vert (appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				half3 rgb = Overlay(col.rgb ,_Color1.rgb);
				half p=	((i.worldPos.y-_Time.w*_Speed) % _Height)/_Height;
				half4 col2 = tex2D(_Noise, half2(i.worldPos.x+i.worldPos.z, i.worldPos.y));
				col.a = _Transp + col2.r / 10;

				if (p > 0.2 && p<0.8) {
					half mult =1- abs(0.5 - p)/0.3;
					mult = pow(1 + mult, _Strength);
					col.a = clamp((_Transp2 - col2.r/5+0.1)*col2.r*mult,_Transp,_Transp2);
					if (col.a > (_Transp + _Transp2) / 2) {
						col.rgb = rgb;
					}	
				}

				

				

				return col;
			}

				

			ENDCG
		}
	}
}
