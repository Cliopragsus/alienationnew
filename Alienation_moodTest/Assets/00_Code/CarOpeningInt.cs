﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarOpeningInt : MonoBehaviour
{
    public Transform destination;
    public GameObject particles;
    private bool crashed = false;

	public void Go ()
    {
        gameObject.GetComponent<NavMeshAgent>().destination = destination.position;
	}

    private void Update()
    {
        if (Vector3.Distance(transform.position,destination.position) < 2 && !crashed)
        {
            StartCoroutine(CarCrash());
            crashed = true;
        }
    }

    public IEnumerator CarCrash()
    {
        Instantiate(particles, transform.position, Quaternion.identity);
        gameObject.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2);
        //gameObject.SetActive(false);
    }
}
