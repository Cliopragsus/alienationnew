﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Q00_EnterRoomTrigger : MonoBehaviour
{
    public void Start()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }

    public void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.instance.quests[1]++;
            GameManager.instance.UpdateQuest();
            gameObject.SetActive(false);
        }
    }
}
