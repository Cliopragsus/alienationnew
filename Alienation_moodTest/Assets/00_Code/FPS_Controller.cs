﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPS_Controller : MonoBehaviour
{
    [Header("Camera")]
    public float sensitivity = 1; //default: 50
    private float rotX;
    private float rotY;

    [Header("PlayerAttributes")]
    public float distFromGround = 3;
    public float playerSpeed = 50;
    private float defaultPlayerSpeed;
    private Rigidbody playerRB;
    public float jumpHeight = 5;

    public float interactionRange = 5;

    [Header("Steps")]
    public AudioClip[] steps;
    private float stepCountH;
    private float stepCountV;
    public float stepLimit = 10;
    private AudioSource aS;

    [Header("Other")]
    public GameObject menu;
    public GameObject interactionHint;
    public GameObject target;
    private delegate void Abilities();
    private List<Abilities> abilityMethods = new List<Abilities>();
    public int currentAbilityIndex;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start ()
    {
		Cursor.lockState = CursorLockMode.Locked;
        //gameObject.GetComponent<MeshRenderer>().enabled = false;
        playerRB = gameObject.GetComponent<Rigidbody>();
        defaultPlayerSpeed = playerSpeed;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        interactionHint = GameObject.Find("InteractionText");
        menu = GameObject.Find("AbilityWheel");
        menu.SetActive(false);
        aS = gameObject.GetComponent<AudioSource>();
        CreateList();
    }

	void Update ()
    {
        // player movement, just moves him, really
        Move();
        Update_FPS_Cam();
        CheckTarget();

        if (Input.GetButtonDown("Jump") && Grounded())
        {
            playerRB.AddForce(Vector3.up * jumpHeight * 100);
            playerSpeed = playerSpeed / 1.5f;
        }

        if (Grounded())//air mobility constraint reset
        {
            playerSpeed = defaultPlayerSpeed;
        }

        if (Input.GetButtonDown("Menu"))//Open Wheel
        {
            menu.SetActive(true);
            Time.timeScale = 0.1f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetButtonUp("Menu"))//CloseWheel
        {
            menu.SetActive(false);
            Time.timeScale = 1.0f;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetButtonDown("AbilityUse"))
        {
            abilityMethods[currentAbilityIndex]();
        }

        Steps();
	}

    public void CheckTarget()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, interactionRange))
        {
            target = hitInfo.collider.gameObject;
            if (target.GetComponent<Interactable>())
            {
                interactionHint.SetActive(true);
                interactionHint.GetComponentInChildren<Text>().text = target.GetComponent<Interactable>().interactionText;
            }
        }
        else
        {
            target = null;
            interactionHint.SetActive(false);
        }
    }

    public void Steps()
    {
        if (Grounded())
        {
            stepCountH += Input.GetAxis("Horizontal");
            stepCountV += Input.GetAxis("Vertical");
        }


        if (stepCountH >= stepLimit || stepCountH <= -stepLimit || stepCountV >= stepLimit || stepCountV <= -stepLimit)
        {
            aS.clip = steps[Random.Range(1, 4)];
            aS.Play();
            stepCountH = 0;
            stepCountV = 0;
        }
    }

    #region Abilites

    void CreateList()
    {
        abilityMethods.Add(Interact);
        abilityMethods.Add(LockPicking);
        abilityMethods.Add(SomethingSomething);
    }
    public void SetAbility(int abilityIndex)
    {
        currentAbilityIndex = abilityIndex;
    }

    // Abilities
    void Interact()
    {
        if (target.GetComponent<Interactable>())
        {
            target.GetComponent<Interactable>().Interact();
            target.GetComponent<Interactable>().Story();
        }
    }
    void LockPicking()
    {
        Debug.Log("Used Lockpicking-Ability");
        if (target.CompareTag("Door"))
        {
            target.GetComponent<Interactable>().Interact();
            currentAbilityIndex = 0;
        }
    }
    void SomethingSomething()
    {
        Debug.Log("Used SomethingSomething-Ability");
    }
    #endregion

    #region PlayerMovement, Cam, Grounded

    private void Move()
    {
        gameObject.transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * playerSpeed, 0, Input.GetAxis("Vertical") * Time.deltaTime * playerSpeed);
    }

    //This does all the camera movement
	public void Update_FPS_Cam()
    {
        rotX = Input.GetAxis("Mouse X") * sensitivity;
        rotY = Input.GetAxis("Mouse Y") * sensitivity;

        gameObject.transform.Rotate(0, rotX, 0);
        Camera.main.gameObject.transform.Rotate(-rotY, 0, 0);
    }

    public bool Grounded()
    {
        if (Physics.Raycast(transform.position, Vector3.down, distFromGround) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
#endregion
}
