﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject player;
    public FPS_Controller pFps;
    public GameObject currentHolo;
    public AssistantInt cAss;

    public int[] quests = new int[] { 0, 0, 0 };

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Start()
    {
        instance = this;
        player = GameObject.Find("Player");
        currentHolo = GameObject.Find("Holo");
        pFps = player.GetComponent<FPS_Controller>();
        cAss = currentHolo.GetComponent<AssistantInt>();
    }

    public void FindHolo()
    {
        StartCoroutine(HoloFinding());
    }

    public IEnumerator HoloFinding()
    {
        yield return new WaitForSeconds(2);
        currentHolo = GameObject.Find("Holo");
        cAss = currentHolo.GetComponent<AssistantInt>();
    }

    public void UpdateQuest()
    {
        cAss.lineIndex = 2;

        if (quests[0] == 1)
        {
            cAss.lines[0] = "You have found the cube, great joy.";
            cAss.lines[1] = "Please do not tinker with the masters accessories";
        }
        if (quests[1] == 1)
        {
            cAss.lines[0] = "Here, have this lockpicking ability for reason x. You're Welcome.";
            pFps.menu.transform.GetChild(1).gameObject.SetActive(true);
        }
        if (quests[1] == 2)
        {
            cAss.lines[0] = "Please be aware that you are in Masters private area. I cannot but ponder how you managed to get in here.";
        }
        if (quests[1] == 3)
        {
            cAss.lines[0] = "You have tinkered with my base.";
            cAss.lines[1] = "For reason Y I am compelled to follow you for all of eternity now.";
            cAss.lines[2] = "Lead me, person.";
        }
    }
}
