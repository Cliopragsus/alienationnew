﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interactable
{
    public bool opened = false;
    private GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (Vector3.Distance(player.transform.position,transform.position) > 8 && opened)
        {
            transform.Rotate(0, -90, 0);
            opened = false;
            Debug.Log("Door Closed");
            gameObject.GetComponent<AudioSource>().Play();
        }
    }

    public override void Interact()
    {
        if (!opened && player.GetComponent<FPS_Controller>().currentAbilityIndex == 1)
        {
            transform.Rotate(0, 90, 0);
            Debug.Log("Door Opened");
            opened = true;
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
