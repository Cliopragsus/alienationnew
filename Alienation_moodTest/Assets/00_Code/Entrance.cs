﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Entrance : Interactable
{
    public int levelIndex;

    public override void Interact()
    {
        SceneManager.LoadScene(levelIndex, LoadSceneMode.Single);
        GameManager.instance.player.transform.position = new Vector3(10, 1, -10);
        GameManager.instance.FindHolo();
        GameManager.instance.GetComponent<AudioSource>().Stop();
    }
}
