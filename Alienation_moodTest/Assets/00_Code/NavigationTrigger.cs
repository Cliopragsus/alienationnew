﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationTrigger : MonoBehaviour
{
    public GameObject car;

    private void Start()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Triggered Navigation");
            car.GetComponent<CarOpeningInt>().Go();
            //GameObject.Find("GameManager").GetComponent<CitizenManager>().NavigateCitizenToCar();
            gameObject.SetActive(false);
        }
    }
}
