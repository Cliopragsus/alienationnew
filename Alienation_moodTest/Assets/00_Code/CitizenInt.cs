﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CitizenInt : MonoBehaviour
{
    public GameObject target;
    public bool readyToFollow;

    public void Update()
    {
        if (target != null)
        {
            if (readyToFollow)
            {
                gameObject.GetComponent<NavMeshAgent>().destination = target.transform.position;
            }
        }
    }
}
