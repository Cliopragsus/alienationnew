﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarInt : MonoBehaviour
{

    public Transform goal;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Citizen"))
        {
            other.gameObject.SetActive(false);
            gameObject.GetComponent<NavMeshAgent>().destination = goal.position;
        }
    }
}
