﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AssistantInt : Interactable
{
    private GameObject player;
    private NavMeshAgent nMA;

    public bool waiting = true;
    public GameObject textBox;
    public string[] lines;
    public int lineIndex;

	void Start ()
    {
        nMA = gameObject.GetComponent<NavMeshAgent>();
        StartCoroutine(IdleChat());
	}
	
	void Update ()
    {
        if (!waiting)
        {
            FindPlayer();
            textBox.transform.LookAt(Camera.main.transform.position);
        }
	}

    public void FindPlayer()
    {
        if (player == null)
        {
            player = GameObject.Find("Player");
        }
        else
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, player.transform.position - transform.position, out hitInfo, Mathf.Infinity))
            {
                nMA.destination = player.transform.position;

                if (hitInfo.collider.gameObject != player)
                {
                    nMA.stoppingDistance = 0;
                    Debug.Log("Cant See Player");
                }
                else
                {
                    Debug.Log("SeeingPlayer");
                    nMA.stoppingDistance = 5;
                }
            }
        }
    }

    public void ContextTalk(string text)
    {
        StartCoroutine(SpecificTalk(text));
    }

    public IEnumerator SpecificTalk(string text)
    {
        waiting = true;
        textBox.GetComponentInChildren<Text>().text = text;
        textBox.SetActive(true);
        yield return new WaitForSeconds(5);
        textBox.SetActive(false);
        waiting = false;
    }

    public IEnumerator IdleChat()
    {
        yield return new WaitForSeconds(Random.Range(1,3));
        if (!waiting)
        {
            textBox.GetComponentInChildren<Text>().text = lines[lineIndex];
            textBox.SetActive(true);
            yield return new WaitForSeconds(5);
            if (!waiting)
            {
                textBox.SetActive(false);
                if (lineIndex >= lines.Length - 1)
                {
                    lineIndex = 0;
                }
                else
                {
                    lineIndex++;
                }
            }
        }
        StartCoroutine(IdleChat());
    }
}
