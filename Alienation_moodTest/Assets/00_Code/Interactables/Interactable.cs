﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{
    public string interactionText;
    public int relatedTo = -1;
    public int controlValue = -1;

    public void Story()
    {
        //Debug.Log("BaseInteractable");
        if (GameManager.instance.currentHolo.GetComponent<Renderer>().isVisible)
        {
            GameManager.instance.currentHolo.GetComponent<AssistantInt>().ContextTalk(interactionText);
        }
        else
        {
            GameManager.instance.pFps.interactionHint.GetComponentInChildren<Text>().text = interactionText;
        }

        if (relatedTo >= 0)
        {
            if (controlValue == GameManager.instance.quests[relatedTo])
            {
                GameManager.instance.quests[relatedTo]++;
                GameManager.instance.UpdateQuest();
            }
        }

        if (gameObject.GetComponent<AudioSource>())
        {
            gameObject.GetComponent<AudioSource>().Play();
        }
    }

    public virtual void Interact()
    {
        Debug.Log("Base Interaction");
    }
}
