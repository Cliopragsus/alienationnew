﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : Interactable
{
    public bool beeingHeld;

    public override void Interact()
    {
        if (beeingHeld)
        {
            beeingHeld = false;
        }
        else
        {
            beeingHeld = true;
        }
    }

    public void Update()
    {
        if (beeingHeld)
        {
            transform.position = Vector3.MoveTowards(transform.position, GameManager.instance.player.transform.GetChild(0).transform.GetChild(0).transform.position, 05f);
        }
    }

}
