﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CitizenManager : MonoBehaviour
{
    private GameObject[] citizen;
    private GameObject[] cars;
    public int controlRange = 5;

    private void Start()
    {
        citizen = GameObject.FindGameObjectsWithTag("Citizen");
        cars = GameObject.FindGameObjectsWithTag("Car");
    }

    public void NavigateCitizenToCar()
    {
        foreach (GameObject guy in citizen)
        {
            foreach (GameObject car in cars)
            {
                if (Vector3.Distance(guy.transform.position,car.transform.position) < controlRange)
                {
                    guy.GetComponent<NavMeshAgent>().destination = car.transform.position;
                }
            }
        }
    }
}
